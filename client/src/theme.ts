import { createTheme } from '@mui/material';

const colors = {
  primary: '#6259E3',
  secondary: '#42E48B',
};

const theme = createTheme({
  typography: {
    fontFamily: ['roboto', 'serif', 'sans-serif'].join(','),
  },
  palette: {
    primary: {
      main: colors.primary,
    },
    secondary: {
      main: colors.secondary,
    },
  },
});

export default theme;
