import { Container, CssBaseline, ThemeProvider } from '@mui/material';
import theme from './theme';
import TodoList from './Todos/TodoList';

const App = () => {
  return (
    <ThemeProvider theme={theme}>
      <CssBaseline>
        <Container maxWidth='md'>
          <TodoList />
        </Container>
      </CssBaseline>
    </ThemeProvider>
  );
};

export default App;
