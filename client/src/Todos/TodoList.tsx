import { useCallback, useState } from 'react';
import useTodos, { Todo } from './useTodos';
import theme from '../theme';
import {
  Typography,
  Box,
  Dialog,
  DialogContent,
  DialogActions,
  Button,
} from '@mui/material';
import Loading from '../Loading';
import TodoItem from './TodoItem';
import TodoInput from './TodoInput';

const TodoList = () => {
  const { todos, isLoading, error, createTodo, updateTodo, deleteTodo } =
    useTodos();

  const [selectedTodo, setSelectedTodo] = useState<Todo>();

  const handleCreateTodo = useCallback(
    (value: string) => {
      const message = value;
      createTodo({ message });
    },
    [createTodo]
  );

  const handleEditTodoMessage = useCallback(
    (value: string) => {
      const message = value;
      updateTodo({ ...selectedTodo, message });
      setSelectedTodo(undefined);
    },
    [selectedTodo, updateTodo]
  );

  const handleUpdateTodo = useCallback(
    (todo: Todo) => {
      updateTodo(todo);
    },
    [updateTodo]
  );

  const handleDeleteTodo = useCallback(
    (uuid: string) => {
      deleteTodo(uuid);
    },
    [deleteTodo]
  );

  if (error) {
    return (
      <Box marginY={theme.spacing(4)}>
        <Typography textAlign='center'>Oops!, something went wrong.</Typography>
      </Box>
    );
  }

  return (
    <>
      <Typography
        component='h1'
        variant='h3'
        textAlign='center'
        sx={{ marginY: theme.spacing(4) }}
      >
        Hi, what do you need to do today?
      </Typography>

      <Box marginBottom={theme.spacing(4)}>
        <TodoInput onSubmit={(value: string) => handleCreateTodo(value)} />
      </Box>

      {todos.map((todo) => (
        <TodoItem
          key={todo.uuid}
          todo={todo}
          handleEditTodoMessage={(todo: Todo) => setSelectedTodo(todo)}
          handleUpdateTodo={(todo: Todo) => handleUpdateTodo(todo)}
          handleDeleteTodo={(uuid: string) => handleDeleteTodo(uuid)}
        />
      ))}
      {!todos.length && (
        <Typography textAlign='center'>
          Looks like you're all caught up.
        </Typography>
      )}

      <Dialog open={!!selectedTodo} maxWidth='md' fullWidth>
        <DialogContent>
          <TodoInput
            todo={selectedTodo}
            onSubmit={(value: string) => handleEditTodoMessage(value)}
          />
        </DialogContent>
        <DialogActions>
          <Button onClick={() => setSelectedTodo(undefined)}>Cancel</Button>
        </DialogActions>
      </Dialog>

      {/* 
        would maybe with more time rethink this loader, the quick flash on update is annoying me.
      */}

      <Loading isLoading={isLoading} />
    </>
  );
};

export default TodoList;
