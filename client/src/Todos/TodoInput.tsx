import { SyntheticEvent, useRef } from 'react';
import { Todo } from './useTodos';
import { TextField } from '@mui/material';

interface TodoInputProps {
  todo?: Todo;
  onSubmit: (value: string) => void;
}

const TodoMessage = ({ todo, onSubmit }: TodoInputProps) => {
  const formRef = useRef<HTMLFormElement>(null);

  const handleSubmit = (e: SyntheticEvent) => {
    e.preventDefault();
    const value = formRef?.current?.message.value;
    formRef?.current?.reset();
    if (value) {
      onSubmit(value);
    }
  };

  return (
    <form ref={formRef} onSubmit={handleSubmit}>
      <TextField
        label={todo?.message ? undefined : 'Add a new item'}
        name='message'
        fullWidth
        autoFocus
        autoComplete='off'
        defaultValue={todo?.message}
      />
    </form>
  );
};

export default TodoMessage;
