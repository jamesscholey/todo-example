import { useCallback, useEffect, useState } from 'react';
import axios from 'axios';

const api = 'http://localhost:3001/todos';

export interface Todo {
  uuid?: string;
  message?: string;
  important?: boolean;
  completed?: boolean;
}

/* 
Custom hook seemed appropriate for a small app like this and a nice way to pull out the business logic, in a larger app I would look at using Context, State Managament and caching.
*/

const useTodos = () => {
  const [todos, setTodos] = useState<Todo[]>([]);
  const [isLoading, setIsLoading] = useState<boolean>(false);
  const [error, setError] = useState();

  const getTodos = useCallback(async () => {
    setIsLoading(true);

    try {
      const { data: todos }: { data: Todo[] } = await axios.get(api);
      setTodos(todos);
    } catch (error: any) {
      setError(error);
    }
    setIsLoading(false);
  }, [setIsLoading, setTodos, setError]);

  useEffect(() => {
    getTodos();
  }, [getTodos]);

  const createTodo = useCallback(
    async (todo: Todo) => {
      setIsLoading(true);

      try {
        const { data: newTodo }: { data: Todo } = await axios.post(api, todo);
        setTodos((prevState) => [...prevState, newTodo]);
      } catch (error: any) {
        setError(error);
      }
      setIsLoading(false);
    },
    [setIsLoading, setTodos, setError]
  );

  const updateTodo = useCallback(
    async (todo: Todo) => {
      setIsLoading(true);

      try {
        const { data: updatedTodo }: { data: Todo } = await axios.put(
          `${api}/${todo.uuid}`,
          todo
        );
        setTodos((prevState) =>
          prevState.map((todo) => {
            if (todo.uuid === updatedTodo.uuid) {
              return updatedTodo;
            }
            return todo;
          })
        );
      } catch (error: any) {
        setError(error);
      }
      setIsLoading(false);
    },
    [setIsLoading, setTodos, setError]
  );

  const deleteTodo = useCallback(
    async (uuid: string) => {
      setIsLoading(true);

      try {
        await axios.delete(`${api}/${uuid}`);
        setTodos((prevState) => prevState.filter((todo) => todo.uuid !== uuid));
      } catch (error: any) {
        setError(error);
      }
      setIsLoading(false);
    },
    [setIsLoading, setTodos, setError]
  );

  return {
    todos,
    isLoading,
    error,
    createTodo,
    updateTodo,
    deleteTodo,
  };
};

export default useTodos;
