import { useCallback } from 'react';
import { Todo } from './useTodos';
import theme from '../theme';
import { Grid, IconButton, Paper, Tooltip, Typography } from '@mui/material';

import CircleOutlinedIcon from '@mui/icons-material/CircleOutlined';
import CheckCircleIcon from '@mui/icons-material/CheckCircle';
import DeleteIcon from '@mui/icons-material/Delete';
import EditIcon from '@mui/icons-material/Edit';
import PriorityHighIcon from '@mui/icons-material/PriorityHigh';

interface TodoItemProps {
  todo: Todo;
  handleEditTodoMessage: (todo: Todo) => void;
  handleUpdateTodo: (todo: Todo) => void;
  handleDeleteTodo: (uuid: string) => void;
}

const TodoItem = ({
  todo,
  handleEditTodoMessage,
  handleUpdateTodo,
  handleDeleteTodo,
}: TodoItemProps) => {
  const handleCompleted = useCallback(() => {
    handleUpdateTodo({ ...todo, completed: !todo.completed });
  }, [handleUpdateTodo, todo]);

  const handleImportant = useCallback(() => {
    handleUpdateTodo({ ...todo, important: !todo.important });
  }, [handleUpdateTodo, todo]);

  return (
    <Paper
      elevation={3}
      sx={{
        padding: theme.spacing(2),
        marginBottom: theme.spacing(2),
        ':last-of-type': { marginBottom: 0 },
      }}
    >
      <Grid container spacing={2} alignItems='center'>
        <Grid item xs={2} sm={1}>
          <Tooltip title='Mark as completed'>
            <IconButton onClick={handleCompleted}>
              {todo.completed ? (
                <CheckCircleIcon color='secondary' />
              ) : (
                <CircleOutlinedIcon />
              )}
            </IconButton>
          </Tooltip>
        </Grid>

        <Grid item xs={10} sm={8}>
          <Typography
            // would maybe create a component here that handles the styles better depending on state. Instead of these nested ternary conditions.
            sx={{
              color: todo.completed
                ? theme.palette.grey[500]
                : todo.important
                ? theme.palette.error.light
                : undefined,
              fontWeight:
                todo.important && !todo.completed
                  ? theme.typography.fontWeightMedium
                  : undefined,
              textDecorationLine: todo.completed ? 'line-through' : undefined,
            }}
          >
            {todo.message}
          </Typography>
        </Grid>

        <Grid item container xs={12} sm={3} justifyContent='end'>
          <Tooltip title='Mark as important'>
            <IconButton
              sx={{ display: todo.completed ? 'none' : undefined }}
              onClick={() => handleImportant()}
            >
              <PriorityHighIcon
                htmlColor={
                  todo.important && !todo.completed
                    ? theme.palette.error.light
                    : undefined
                }
              />
            </IconButton>
          </Tooltip>
          <Tooltip title='Edit todo'>
            <IconButton
              sx={{ display: todo.completed ? 'none' : undefined }}
              onClick={() => handleEditTodoMessage(todo)}
            >
              <EditIcon />
            </IconButton>
          </Tooltip>
          <Tooltip title='Delete todo'>
            <IconButton onClick={() => handleDeleteTodo(todo.uuid!)}>
              <DeleteIcon />
            </IconButton>
          </Tooltip>
        </Grid>
      </Grid>
    </Paper>
  );
};

export default TodoItem;
