# Todo Example

## Installation

Install dependencies

```bash
cd server && npm i
cd client && npm i
```

## Usage

start server (PORT 3001)

```bash
cd server && npm start
```

start client (PORT 3000)

```bash
cd client && npm start
```
