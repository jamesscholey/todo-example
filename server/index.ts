import express from 'express';
import cors from 'cors';
import { v4 as uuidv4, validate } from 'uuid';

const app = express();

// would usually add process.env.PORT || here
const PORT = 3001;

app.use(cors());
app.use(express.json());

interface Todo {
  uuid: string;
  message: string;
  important?: boolean;
  completed?: boolean;
}

// 😉
let todos: Todo[] = [
  { uuid: uuidv4(), message: 'Hire James?', important: true, completed: false },
];

/* 
I added some basic validations and checks in these methods.
If I had more time to add database functionality, most of these would be taken
care of there. In the case of a more complex api, I would usually split out the
routes and business logic into respective files and services for better separation of concerns.

With more time I would have also liked to implement multiple todo lists, and filtering of ordering by non completed -> completed
*/

app.get('/todos', (_req, res) => {
  res.status(200).json(todos);
});

app.post('/todos', (req, res) => {
  const { message } = req.body as Todo;

  if (!message?.length) {
    return res
      .status(422)
      .json({ error: `Oops, seems like you didn't enter a message.` });
  }

  const newTodo: Todo = { uuid: uuidv4(), message: message.trim() };

  todos.push(newTodo);

  res.status(200).json(newTodo);
});

// Maybe I could have used seperate PATCH routes for toggling the important and completed values.
app.put('/todos/:uuid', (req, res) => {
  const { uuid } = req.params;
  const { message, important, completed } = req.body as Todo;

  if (!validate(uuid)) {
    return res.status(422).json({ error: `Oops, something went wrong.` });
  }

  if (!message?.length) {
    return res
      .status(422)
      .json({ error: `Oops, seems like you didn't enter a message.` });
  }

  const todo = todos.find((todo) => todo.uuid === uuid);

  if (!todo) {
    return res.status(404).json({ error: `Oops, we can't seem to find that.` });
  }

  todo.message = message.trim();
  todo.important = important ?? false;
  todo.completed = completed ?? false;

  res.status(200).json(todo);
});

app.delete('/todos/:uuid', (req, res) => {
  const { uuid } = req.params;

  if (!validate(uuid)) {
    return res.status(422).json({ error: `Oops, something went wrong.` });
  }

  todos = todos.filter((todo) => todo.uuid !== uuid);

  res.status(200).json(null);
});

app.listen(PORT, () => {
  console.log(`⚡ Server running at https://localhost:${PORT} `);
});
